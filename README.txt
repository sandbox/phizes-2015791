CAUTION
-------

Please Note: Use Of This Module May Break Your Site In Future

When a fix for this issue gets committed to either entitycache or profile2, this
module may break your site. I take no responsibility for this and it is up to
you to ensure this does not happen. The current issue to follow seems to be
https://drupal.org/node/1387268

INTRODUCTION
------------

This module only has one purpose currently, it creates the
'cache_entity_profile2' cache table so that profile2 can use entitycache.
Currently, installing profile2 and entitycache will result in errors when a
user tries to cancel their account. This module fixes that, and allows profile2
entities to be cached. This module will only really be needed when using the
database as the cache backend, which is the default.

This module (entity_cache_profile2) currently lives at:
https://drupal.org/sandbox/Phizes/2015791 

INSTALLATION
------------

Install as any other Drupal module. Generally you would place the module folder
in sites/all/modules or sites/example.com/modules for a specific site. It
depends on entitycache, profile2 and consequently entity.

CONFIGURATION
-------------

There is no configuration, enable the module to create the database table, and
uninstall it to remove the table.

MAINTAINER
----------

Phizes https://drupal.org/user/1093080
